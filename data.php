<?php

error_reporting(0);

require_once "conn.php";

$get_lat = is_numeric($_REQUEST['lat']) ? $_REQUEST['lat'] : 0;
$get_lng = is_numeric($_REQUEST['lng']) ? $_REQUEST['lng'] : 0;
$get_dist = is_numeric($_REQUEST['dist']) ? $_REQUEST['dist'] : 10;

/*
  Choose all stations according to user coordinates and distance:

  a = sin²(Δφ/2) + cos φ1 ⋅ cos φ2 ⋅ sin²(Δλ/2)
  c = 2 ⋅ atan2( √a, √(1−a) )
  d = R ⋅ c
  where	φ is latitude, λ is longitude, R is earth’s radius (mean radius = 6,371km);
  (https://www.movable-type.co.uk/scripts/latlong-nomodule.html)

  The average Earth radius is 3,959 mi
  (https://en.wikipedia.org/wiki/Earth_radius)

  go through bikes in there selecting the latest `end_time` (MAX(tripdata.end_time));
  group and count by `end_station_id` (bikes count on each station) (count(*) AS bikeCount)
  order by ascending distance by default
*/

	    $result = mysqli_query($sql_conn, "
				SELECT f.end_station_id,
				      f.end_station_name,
				      distance,
				      count(*) AS bikeCount
				FROM
				  (SELECT tripdata.end_time,
					  tripdata.end_station_name,
					  tripdata.bike_id,
					  end_station_id,
					  (3959 * acos(cos(radians($get_lat)) * cos(radians(end_station_latitude)) * cos(radians(end_station_longitude) - radians($get_lng)) + sin(radians($get_lat)) * sin(radians(end_station_latitude)))) AS distance
				  FROM tripdata
				  INNER JOIN
				    (SELECT end_station_name,
					    MAX(tripdata.end_time) AS TopDate
				      FROM tripdata
				      GROUP BY bike_id) AS EachItem ON EachItem.TopDate = tripdata.end_time
				  AND EachItem.end_station_name = tripdata.end_station_name
				  HAVING distance < $get_dist
				  ORDER BY distance,
					    end_time) f
				GROUP BY end_station_id
				ORDER BY distance ASC
				      ");

  $json_array = array();

  // could also json_encode directly from mysqli_fetch_array with a little changes in query

  while ($row = mysqli_fetch_object($result)) {

  $distance_km = round($row->distance*1.609344, 1);
  $distance_mi = round($row->distance, 1);
  
     $json_array[] = array(
			(int)$row->end_station_id,
			$row->end_station_name,
			$distance_mi,
			$row->bikeCount
			);
  }

  print json_encode(array("bikesData"=>$json_array));
  
?>