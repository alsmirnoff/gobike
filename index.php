<!doctype html>
<html lang="en">
    <head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">

		<title>Ford GoBike</title>

		<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>

		<script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
		
	<script>
	  $(document).ready( function () {
	  var datatable;
	  
	  getTableData();
	  
	    $("#goSearch").on("click", function(){
	      getTableData();
	    });
	  });
	  
	  getTableData = function(){
	    if ( $.fn.dataTable.isDataTable( '.GoBikeTable' ) ) {
			$('.GoBikeTable').DataTable().destroy();
	    }
	    this.datatable = $('.GoBikeTable').DataTable( {
			"language": {
			  "emptyTable": "Niestety nie znaleźliśmy dostępnych rowerów w Twojej okolicy"
			},
			"order": [[ 2, "asc" ]],
			"columnDefs": [
			    { "width": "15%", "targets": 0 },
			    { "width": "60%", "targets": 1 },
			    { "width": "15%", "targets": 2 },
			    { "width": "10%", "targets": 2 }
			  ],

			ajax: {
			    url: "data.php",
			    dataSrc: "bikesData",
			    data: function ( d ) {
				d.lat = $("#LatitudeVal").val();
				d.lng = $("#LongitudeVal").val();
				d.dist = $("#distanceVal").val();
			      }
			}
	    });
	  }
	</script>
    </head>
<body>

<div class="col-md-6 offset-md-3" style="padding:20px;">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Ford_GoBike_logo.svg/1280px-Ford_GoBike_logo.svg.png" style="width:30%;padding:20px;">
  <div class="jumbotron">

	<div class="row">
	
	    <div class="col">
		<label for="exampleFormControlSelect1">Within range, mi:</label>
		<select class="form-control" id="distanceVal">
		  <option value=10>20</option>
		  <option value=10>10</option>
		  <option value=5>5</option>
		  <option value=2>2</option>
		  <option value=1>1</option>
		</select>
	    </div>
	    <div class="col">
	      <label for="LatitudeVal">Latitude</label>
	      <input type="text" class="form-control" placeholder="Latitude" id="LatitudeVal" value="37.7849">
	    </div>
	    <div class="col">
	      <label for="LongitudeVal">Longitude</label>
	      <input type="text" class="form-control" placeholder="Longitude" id="LongitudeVal" value="-122.121">
	    </div>

	  </div>
	  
	  <div class="row">
	    <div class="col-sm-12 text-center p-3">
	      <button type="submit" class="btn btn-primary" id="goSearch">Search</button>
	    </div>
	  </div>

  </div>

<table class="GoBikeTable dataTable table table-striped">
        <thead>
            <tr>
                <th scope="col">Station #</th>
                <th scope="col">Name</th>
		<th scope="col">Distance, mi</th>
		<th scope="col">Bikes</th>
<!-- 		<th scope="col">km</th> -->
            </tr>
        </thead>
</table>

</div>

</body>
</html>
